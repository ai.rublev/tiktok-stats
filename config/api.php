<?php

return [
    'clien_key' => 'vBQgSJoAWjF5kterbyL6Xxf0iUsw1nh9',
    'tiktok' => [
        'user_info_rules' => [
            'statusCode' => 'required|integer',
            'userInfo' => 'required|array',

            'userInfo.stats' => 'required|array',
            'userInfo.stats.followerCount' => 'required|integer',
            'userInfo.stats.followingCount' => 'required|integer',
            'userInfo.stats.heartCount' => 'required|integer',
            'userInfo.stats.videoCount' => 'required|integer',

            'userInfo.user' => 'required|array',
            'userInfo.user.avatarMedium' => 'required|string',
            'userInfo.user.avatarThumb' => 'required|string',
            'userInfo.user.id' => 'required|integer',
            'userInfo.user.nickname' => 'string',
            'userInfo.user.secUid' => 'required|string',
            'userInfo.user.signature' => 'string',
            'userInfo.user.uniqueId' => 'required|string',
        ],
        'user_videos_rules' => [
            'hasMore' => 'required|boolean',
            'maxCursor' => 'required|integer',
            'minCursor' => 'required|integer',
            'statusCode' => 'required|integer',

            'items' => 'array',
            'items.*.createTime' => 'required|integer',
            'items.*.desc' => 'string',
            'items.*.id' => 'required|integer',

            'items.*.stats' => 'required|array',
            'items.*.stats.commentCount' => 'required|integer',
            'items.*.stats.diggCount' => 'required|integer',
            'items.*.stats.playCount' => 'required|integer',
            'items.*.stats.shareCount' => 'required|integer',

            'items.*.video' => 'required|array',
            'items.*.video.cover' => 'required|string',
            'items.*.video.downloadAddr' => 'required|string',
        ],
    ],
    'errors' => [
        'api_key_incorrect' => [
            'response' => [
                'success' => false,
                'statusCode' => 1,
                'error' => 'The client does not have a api key'
            ],
            'response_code' => 401,
        ],
        'user_not_found' => [
            'response' => [
                'success' => false,
                'statusCode' => 2,
                'error' => 'User does not exist'
            ],
            'response_code' => 404,
        ],
    ],
];
