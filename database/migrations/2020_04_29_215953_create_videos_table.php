<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('tiktok_id');
            $table->unsignedInteger('user_id');
            $table->unique(['tiktok_id', 'user_id'], 'unique_video');
            $table->string('url_tiktok', 1000);
            $table->string('url', 1000);
            $table->string('cover_tiktok', 1000);
            $table->string('cover', 1000);
            $table->string('text')->nullable();
            $table->unsignedInteger('comments')->default(0);
            $table->unsignedInteger('likes')->default(0);
            $table->unsignedInteger('plays')->default(0);
            $table->unsignedInteger('shares')->default(0);
            $table->timestamp('create_time');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
