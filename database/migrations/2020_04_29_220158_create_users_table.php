<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('tiktok_id')
                ->unique('unique_user');
            $table->string('secUid', 1000);
            $table->string('login');
            $table->string('link', 300);
            $table->string('nickname')->nullable();
            $table->string('bio', 255)->nullable();
            $table->string('avatar_thumb', 1000);
            $table->string('avatar_medium', 1000);
            $table->string('avatar', 1000);
            $table->unsignedInteger('following')->default(0);
            $table->unsignedInteger('followers')->default(0);
            $table->unsignedInteger('followers_lost')->default(0);
            $table->unsignedInteger('followers_gained')->default(0);
            $table->unsignedInteger('likes')->default(0);
            $table->unsignedInteger('videos')->default(0);
            $table->dateTime('last_visit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
