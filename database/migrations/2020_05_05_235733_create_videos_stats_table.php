<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos_stats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('video_id');
            $table->unsignedInteger('comments')->default(0);
            $table->unsignedInteger('likes')->default(0);
            $table->unsignedInteger('plays')->default(0);
            $table->unsignedInteger('shares')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos_stats');
    }
}
