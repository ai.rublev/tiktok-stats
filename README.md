Deployment:

1. git clone master
2. composer install
3. sudo npm install
4. Redirect to public Directory
5. Fill .env.example (DB connection, QUEUE_CONNECTION=database) file and rename to .env
6. Perform database migrations (php artisan migrate)
7. Set up supervisor on "php artisan queue:work --queue=update_user_videos --tries=2"
