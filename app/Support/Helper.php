<?php

namespace App\Support;

class Helper
{
    public static function convertObjectToArray($obj)
    {
        return json_decode(json_encode($obj), true);
    }
}
