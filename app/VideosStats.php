<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideosStats extends Model
{
    const TABLE_NAME = 'videos_stats';

    protected $fillable = ['comments', 'likes', 'plays', 'shares'];
}
