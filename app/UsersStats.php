<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersStats extends Model
{
    protected $fillable = ['following', 'followers', 'likes', 'videos'];
}
