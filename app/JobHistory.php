<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobHistory extends Model
{
    protected $table = 'jobs_history';

    const QUEUE_STATUS = 'queue';
    const PROCESSING_STATUS = 'processing';
    const DELETED_STATUS = 'deleted';
    const FAILED_STATUS = 'failed';
    const DONE_STATUS = 'done';
}
