<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\TikTokService;
use App\User;
use App\JobHistory;

class UpdateUserVideos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const queueName = 'update_user_videos';

    public $timeout = 0;

    protected $user;
    protected $history;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, JobHistory $history)
    {
        $this->user = $user;
        $this->history = $history;

        // Set up our new history record
        $this->history->item_id = $this->user->id;
        $this->history->job_name = self::queueName;
        $this->history->status = JobHistory::QUEUE_STATUS;

        $this->history->save();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Stop processing if task deleted
        if ($this->history->status == JobHistory::DELETED_STATUS) {
            $this->removeHistory();
            return;
        }

        $this->history->status = JobHistory::PROCESSING_STATUS;
        $this->history->save();

        $tiktok = new TikTokService();
        if ($tiktok->updateUserVideos($this->user, $this->history) == JobHistory::DELETED_STATUS) {
            $this->removeHistory();
            return;
        }

        // Task completed
        $this->history->status = JobHistory::DONE_STATUS;
        $this->history->save();
    }

    public function failed(\Exception $exception)
    {
        $this->history->status = JobHistory::FAILED_STATUS;
        $this->history->save();
    }

    private function removeHistory()
    {
        $this->history->delete();
    }
}
