<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class User extends Model
{
    protected $hidden = ['updated_at'];
    protected $casts = [
        'last_visit' => 'datetime:d.m.Y H:i:s',
    ];

    public function stats()
    {
        return $this->hasMany('App\UsersStats');
    }

    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d.m.Y H:i:s');
    }
}
