<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;

class Video extends Model
{
    const VIDEO_ACTIVE = 1;
    const VIDEO_INACTIVE = 0;
    const TABLE_NAME = 'videos';

    protected $hidden = ['updated_at'];
    protected $casts = [
        'create_time' => 'datetime:d.m.Y H:i:s',
    ];

    public function stats()
    {
        return $this->hasMany('App\VideosStats');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('d.m.Y H:i:s');
    }
}
