<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('key') != config('api.clien_key')) {
            return response()->json(
                config('api.errors.api_key_incorrect.response'),
                config('api.errors.api_key_incorrect.response_code')
            );
        }
        return $next($request);
    }
}
