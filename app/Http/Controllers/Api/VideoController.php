<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\TikTokService;
use App\Jobs\UpdateUserVideos;
use App\User;
use App\Video;
use App\JobHistory;

class VideoController extends Controller
{
    const VIDEOS_DEFAULT_LIMIT = 15;
    const VIDEOS_DEFAULT_OFFSET = 0;
    const VIDEOS_DEFAULT_SORT_BY = 'create_time';
    const VIDEOS_DEFAULT_SORT_TYPE = 'DESC';

    protected $tiktok;

    public function __construct(TikTokService $tiktok)
    {
        $this->tiktok = $tiktok;
    }

    public function getUserVideos(Request $request, $user_id)
    {
        $user = User::find($user_id);

        if (!$user) {
            return response()->json(
                config('api.errors.user_not_found.response'),
                config('api.errors.user_not_found.response_code')
            );
        }

        // validation
        $validator = Validator::make($request->all(), [
            'limit' => 'integer|min:1|max:50',
            'offset' => 'integer|min:0',
            'sort_by' => 'in:create_time,comments,likes,plays,shares',
            'sort_type' => 'in:desc,asc,DESC,ASC'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'statusCode' => 3,
                'error' => 'Some parameters is incorrect'
            ], 400);
        }

        $data = $validator->validated();
        $limit = $data['limit'] ?? self::VIDEOS_DEFAULT_LIMIT;
        $offset = $data['offset'] ?? self::VIDEOS_DEFAULT_OFFSET;
        $sort_by = $data['sort_by'] ?? self::VIDEOS_DEFAULT_SORT_BY;
        $sort_type = $data['sort_type'] ?? self::VIDEOS_DEFAULT_SORT_TYPE;

        // get videos
        $videos = $user->videos()
            ->where('active', Video::VIDEO_ACTIVE)
            ->orderBy($sort_by, $sort_type)
            ->offset($offset)
            ->limit($limit)
            ->get();

        return response()->json([
            'success' => true,
            'statusCode' => 0,
            'data' => $videos,
            'videoCount' => $user->videos()->count()
        ], 200);
    }

    public function updateUserVideos(Request $request, $user_id)
    {
        $user = User::find($user_id);

        if (!$user) {
            return response()->json(
                config('api.errors.user_not_found.response'),
                config('api.errors.user_not_found.response_code')
            );
        }

        $jobHistory = JobHistory::where('job_name', UpdateUserVideos::queueName)
            ->where('item_id', $user->id)
            ->whereIn('status', [JobHistory::QUEUE_STATUS, JobHistory::PROCESSING_STATUS])
            ->first();

        if ($jobHistory) {
            // Delete old task
            $jobHistory->status = JobHistory::DELETED_STATUS;
            $jobHistory->save();
        }

        // Create new task
        UpdateUserVideos::dispatch($user, new JobHistory)
            ->onQueue(UpdateUserVideos::queueName);

        return response()->json([
            'success' => true,
            'statusCode' => 3,
            'message' => 'The queue has been set.'
        ], 200);
    }

    public function updateUserVideosStatus(Request $request, $user_id)
    {
        $user = User::find($user_id);

        if (!$user) {
            return response()->json(
                config('api.errors.user_not_found.response'),
                config('api.errors.user_not_found.response_code')
            );
        }

        $jobHistory = JobHistory::where('job_name', UpdateUserVideos::queueName)
            ->where('item_id', $user->id)
            ->whereIn('status', [JobHistory::QUEUE_STATUS, JobHistory::PROCESSING_STATUS])
            ->first();

        if (!$jobHistory) {
            return response()->json([
                'success' => true,
                'statusCode' => 3,
                'error' => 'Queue does not exist.'
            ], 200);
        }

        return response()->json([
            'success' => true,
            'statusCode' => 4,
            'error' => 'Queue set.'
        ], 200);
    }
}
