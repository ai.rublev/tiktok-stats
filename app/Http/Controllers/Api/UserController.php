<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TikTokService;
use App\User;

class UserController extends Controller
{
    protected $tiktok;

    public function __construct(TikTokService $tiktok)
    {
        $this->tiktok = $tiktok;
    }

    public function getUser(Request $request, $login)
    {
        // Get info about user from tiktok
        $user = $this->tiktok->getUserData($login);

        // Get info from DB if tiktok error
        if (!$user) {
            $user = User::firstWhere('login', $login);

            if (!$user) {
                return response()->json(
                    config('api.errors.user_not_found.response'),
                    config('api.errors.user_not_found.response_code')
                );
            }
        }

        return response()->json([
            'success' => true,
            'statusCode' => 0,
            'data' => $user->toArray()
        ], 200);
    }
}
