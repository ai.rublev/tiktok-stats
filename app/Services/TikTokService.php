<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Video;
use App\JobHistory;

class TikTokService
{
    const API_USER_INFO_URL = 'https://m.tiktok.com/api/user/detail/?uniqueId={login}&language=ru';
    const API_USER_VIDEO_URL = 'https://m.tiktok.com/api/item_list/?count=100&id={tiktok_user_id}&type=1&secUid={secUid}&maxCursor={maxCursor}&minCursor=0&sourceType=8&appId=1233&language=ru';

    const USER_URL = 'https://www.tiktok.com/@{login}';
    const VIDEO_URL = 'https://www.tiktok.com/@{login}/video/{video_id}';

    const CREATED_DIRECTORIES_RULES = 0777;
    const IMG_SAVE_DEFAULT_EXTENSION = '.jpeg';
    const USER_AVATAR_SAVE_URL = 'img/users/{tiktok_user_id}';
    const USER_AVATAR_NAME = 'avatar{img_extension}';
    const VIDEO_COVER_SAVE_URL = 'img/users/{tiktok_user_id}/videos/{tiktok_video_id}';
    const VIDEO_COVER_NAME = 'cover{img_extension}';

    public function getUserData($login)
    {
        $url = str_replace('{login}', $login, self::API_USER_INFO_URL);
        $url = $this->addSignature($url);

        $result = $this->request($url);
        if ($result->getStatusCode() !== 200) {
            return false;
        }

        // validation
        $data = json_decode($result->getBody(), true);
        if (!is_array($data) || Validator::make($data, config('api.tiktok.user_info_rules'))->fails()) {
            return false;
        }

        // record data
        return $this->recordUserToDB($data['userInfo']);
    }

    private function recordUserToDB($data)
    {
        $dataStats = $data['stats'];
        $dataUser = $data['user'];

        $user = User::firstWhere('tiktok_id', $dataUser['id']);

        $userCreated = false;
        if ($user === null) {
            $user = new User();
            $user->tiktok_id = $dataUser['id'];

            $userCreated = true;
        }

        // update user info
        $user->secUid = $dataUser['secUid'];
        $user->login = $dataUser['uniqueId'];
        $user->link = str_replace('{login}', $dataUser['uniqueId'], self::USER_URL);
        if (isset($dataUser['nickname']) && trim($dataUser['nickname']) != '') {
            $user->nickname = $dataUser['nickname'];
        }
        if (isset($dataUser['signature']) && trim($dataUser['signature']) != '') {
            $user->bio = $dataUser['signature'];
        }
        if ($user->avatar_thumb != $dataUser['avatarThumb']) {
            $user->avatar_thumb = $dataUser['avatarThumb'];
            $user->avatar_medium = $dataUser['avatarMedium'];

            $user->avatar = $this->saveUserAvatar($dataUser['id'], $dataUser['avatarThumb']);
        }
        $user->following = $dataStats['followingCount'];
        $user->followers = $dataStats['followerCount'];
        $user->likes = $dataStats['heartCount'];
        $user->videos = $dataStats['videoCount'];
        $user->last_visit = new \DateTime();
        if (!$userCreated) {
            $lastVisitStat = $user->stats()
                ->latest()
                ->first();

            $followersDiff = $user->followers - $lastVisitStat->followers;
            if ($followersDiff > 0) {
                $user->followers_gained += $followersDiff;
            } else if ($followersDiff < 0) {
                $user->followers_lost += abs($followersDiff);
            }
        }

        $user->save();

        // Create stat
        $user->stats()->create([
            'following' => $dataStats['followingCount'],
            'followers' => $dataStats['followerCount'],
            'likes' => $dataStats['heartCount'],
            'videos' => $dataStats['videoCount'],
        ]);

        if ($userCreated) {
            $user->refresh();
        }

        return $user;
    }

    private function addSignature($url)
    {
        $data = json_decode(exec('curl -d "' . $url . '" http://localhost:8080/signature'), true);

        return $url . 
            '&verifyFp=' . $data['verifyFp'] .
            '&_signature=' . $data['signature'];
    }

    private function request($url)
    {
        $client = new Client([
            'http_errors' => false,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
            ],
        ]);

        return $client->get($url);
    }

    private function saveUserAvatar($tiktokUserId, $url)
    {
        $extension = $this->getImgExtensionFromUrl($url);

        return $this->saveImgFromUrl(
            str_replace('{tiktok_user_id}', $tiktokUserId, self::USER_AVATAR_SAVE_URL),
            str_replace('{img_extension}', $extension, self::USER_AVATAR_NAME),
            $url
        );
    }

    private function getImgExtensionFromUrl($url)
    {
        $extension = preg_match('/\.[0-9a-z]{1,5}$/', $url, $matches);
        if (empty($matches)) {
            $extension = self::IMG_SAVE_DEFAULT_EXTENSION;
        } else {
            $extension = $matches[0];
        }

        return $extension;
    }

    private function saveImgFromUrl($saveUrl, $filename, $fromUrl)
    {
        $filename = '/' . $saveUrl . '/' . $filename;
        $uploadFilePath = base_path() . '/public' . $filename;
        $checkFilePath = base_path() . '/public/' . $saveUrl;

        if (!file_exists($checkFilePath)) {
            mkdir($checkFilePath, self::CREATED_DIRECTORIES_RULES, true);
        }

        file_put_contents($uploadFilePath, file_get_contents($fromUrl));

        return url('/') . $filename;
    }

    public function updateUserVideos(User $user, JobHistory $jobHistory, $maxCursor = 0)
    {
        $tiktok_id = $user->tiktok_id;
        $secUid = $user->secUid;

        // Inactivating all user videos, then activating uploaded videos
        if ($jobHistory->status != JobHistory::DELETED_STATUS) {
            $user->videos()
                ->update(['active' => Video::VIDEO_INACTIVE]);
        }

        // Get all user videos by parts
        while (true) {

            // Stop processing if task deleted
            $jobHistory->refresh();
            if ($jobHistory->status == JobHistory::DELETED_STATUS) {
                return JobHistory::DELETED_STATUS;
            }

            $url = str_replace(
                ['{tiktok_user_id}', '{secUid}', '{maxCursor}'], [$tiktok_id, $secUid, $maxCursor],
                self::API_USER_VIDEO_URL
            );
            $url = $this->addSignature($url);

            $result = $this->request($url);
            if ($result->getStatusCode() !== 200) {
                return false;
            }

            // validation
            $data = json_decode($result->getBody(), true);
            if (!is_array($data) || Validator::make($data, config('api.tiktok.user_videos_rules'))->fails()) {
                return false;
            }

            // record videos to DB
            if (isset($data['items']) && !empty($data['items'])) {
                $this->recordVideosToDB($user, $data['items']);
            }

            if ($data['hasMore']) {
                $maxCursor = $data['maxCursor'];
            } else {
                break;
            }
        }

        return true;
    }

    private function recordVideosToDB(User $user, $data)
    {
        foreach ($data as $dataVideo) {
            $video = $user->videos()->firstWhere('tiktok_id', $dataVideo['id']);

            $videoCreated = false;
            if ($video === null) {
                $video = new Video();
                $video->tiktok_id = $dataVideo['id'];
                $video->create_time = (new \DateTime())->setTimestamp($dataVideo['createTime']);

                $videoCreated = true;
            }

            // update video info
            $video->url_tiktok = str_replace(['{login}', '{video_id}'], [$user->login, $dataVideo['id']],
                self::VIDEO_URL
            );
            $video->url = $dataVideo['video']['downloadAddr'];
            if ($video->cover_tiktok != $dataVideo['video']['cover']) {
                $video->cover_tiktok = $dataVideo['video']['cover'];
                $video->cover = $this->saveVideoCover($user->tiktok_id, $dataVideo['id'], $dataVideo['video']['cover']);
            }
            if (isset($dataVideo['desc']) && trim($dataVideo['desc']) != '') {
                $video->text = $dataVideo['desc'];
            }
            $video->comments = $dataVideo['stats']['commentCount'];
            $video->likes = $dataVideo['stats']['diggCount'];
            $video->plays = $dataVideo['stats']['playCount'];
            $video->shares = $dataVideo['stats']['shareCount'];
            $video->active = Video::VIDEO_ACTIVE;

            $user->videos()->save($video);

            // Create initial stat
            if ($videoCreated) {
                $video->stats()->create([
                    'comments' => $dataVideo['stats']['commentCount'],
                    'likes' => $dataVideo['stats']['diggCount'],
                    'plays' => $dataVideo['stats']['playCount'],
                    'shares' => $dataVideo['stats']['shareCount'],
                ]);
            }
        }

        return true;
    }

    /*private function recordVideosToDB(User $user, $data)
    {
        $videos = DB::table(Video::TABLE_NAME)
            // need to keep right sorting of fields according with cycle below
            ->select(
                'tiktok_id', 'user_id', 'create_time', 'created_at', 'url_tiktok', 'url',
                'cover_tiktok', 'cover', 'text', 'comments', 'likes', 'plays', 'shares', 'active', 'updated_at'
            )
            ->where('user_id', $user->id)
            ->whereIn('tiktok_id', array_column($data, 'id'))
            ->get();
        $videos = Helper::convertObjectToArray($videos);
        $newVideos = [];

        $curDateTime = new \DateTime();
        $videosTiktokIds = array_column($videos, 'tiktok_id');
        foreach ($data as $dataVideo) {
            $videoIndex = array_search($dataVideo['id'], $videosTiktokIds);

            if ($videoIndex !== false) {
                $videoCreated = false;
                $video        = $videos[$videoIndex];
            } else {
                $videoCreated = true;
                $video                = [];
                $video['tiktok_id']   = $dataVideo['id'];
                $video['user_id']     = $user->id;
                $video['create_time'] = (new \DateTime())->setTimestamp($dataVideo['createTime']);
                $video['created_at']  = $curDateTime;
            }

            // update video info
            $video['url_tiktok'] = str_replace(['{login}', '{video_id}'], [$user->login, $dataVideo['id']],
                self::VIDEO_URL
            );

            $video['url']        = $dataVideo['video']['downloadAddr'];
            if ($videoCreated || $video['cover_tiktok'] != $dataVideo['video']['cover']) {
                $video['cover_tiktok'] = $dataVideo['video']['cover'];
                $video['cover']        = $this->saveVideoCover($user->tiktok_id, $dataVideo['id'],
                    $dataVideo['video']['cover']
                );
            }

            $video['text']       = $dataVideo['desc'] ?? null;
            $video['comments']   = $dataVideo['stats']['commentCount'];
            $video['likes']      = $dataVideo['stats']['diggCount'];
            $video['plays']      = $dataVideo['stats']['playCount'];
            $video['shares']     = $dataVideo['stats']['shareCount'];
            $video['active']     = Video::VIDEO_ACTIVE;
            $video['updated_at'] = $curDateTime;

            if ($videoCreated) {
                $newVideos[] = $video;
            } else {
                $videos[$videoIndex] = $video;
            }
        }

        // Update videos
        DatabaseHelper::insertOrUpdate(Video::TABLE_NAME, array_merge($videos, $newVideos),
            ['user_id', 'tiktok_id', 'create_time', 'created_at']
        );

        // Create initial stats for new videos
        if (!empty($newVideos)) {
            $newVideosIds = $user->videos()
                ->whereIn('tiktok_id', array_column($newVideos, 'tiktok_id'))
                ->select('id', 'tiktok_id')
                ->get();
            $newVideosIds = $newVideosIds->pluck('id', 'tiktok_id')->toArray();

            $newVideosStats = [];
            foreach ($newVideos as $newVideo) {
                $newVideosStats[] = [
                    'video_id'   => $newVideosIds[$newVideo['tiktok_id']],
                    'comments'   => $newVideo['comments'],
                    'likes'      => $newVideo['likes'],
                    'plays'      => $newVideo['plays'],
                    'shares'     => $newVideo['shares'],
                    'created_at' => $curDateTime,
                    'updated_at' => $curDateTime,
                ];
            }

            DB::table(VideosStats::TABLE_NAME)->insert($newVideosStats);
        }

        return true;
    }*/

    private function saveVideoCover($tiktokUserId, $tiktokVideoId, $url)
    {
        $extension = $this->getImgExtensionFromUrl($url);

        return $this->saveImgFromUrl(
            str_replace(['{tiktok_user_id}', '{tiktok_video_id}'], [$tiktokUserId, $tiktokVideoId],
                self::VIDEO_COVER_SAVE_URL
            ),
            str_replace('{img_extension}', $extension, self::VIDEO_COVER_NAME),
            $url
        );
    }
}
