<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user/{login}', 'Api\UserController@getUser');

Route::get('/user/{user_id}/videos', 'Api\VideoController@getUserVideos');
Route::get('/user/{user_id}/videos/update', 'Api\VideoController@updateUserVideos');
Route::get('/user/{user_id}/videos/update/status', 'Api\VideoController@updateUserVideosStatus');
